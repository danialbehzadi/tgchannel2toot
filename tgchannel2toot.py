#! /usr/bin/env python3
# Danial Behzadi - 2018
# Released under GPLV3+

import config
import os
from pyrogram import Client
from mastodon import Mastodon

def get_text_and_media(message):
    text = " "
    media = None
    if message['text']:
        text = message['text']
    elif message['media']:
        text = message['caption']
        media = message.download()
    return text, media

def connect():
    mastodon = Mastodon(
            access_token = config.access_token,
            api_base_url = config.instance
            )
    return mastodon

def main():
    telegram = Client("tgchannel2toot")
    mastodon = connect()

    @telegram.on_message()
    def check_message(client, message):
        chat = message['chat']

        if chat['type'] == 'channel':
            if chat['username'] == config.channel_id:
                text, media = get_text_and_media(message)
                if media:
                    media_id = mastodon.media_post(media)
                    mastodon.status_post(text, media_ids=media_id)
                    os.remove(media)
                else:
                    mastodon.toot(text)

    telegram.start()
    telegram.idle()

if __name__ == '__main__':
    main()
