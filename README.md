This is a project to automatically send messages in a Telegram channel as a Mastodon toot.

Install
=======
To make it work, do the following:

    $ git clone https://gitlab.com/danialbehzadi/tgchannel2toot.git
    $ cd tgchannel2toot
    $ virtualenv -p python3 --no-site-packages --distribute .env
    $ source .env/bin/activate
    (.env)$ pip3 install -r requirements.txt

Config
======
Make a copy of each config file:

    (.env)$ cp config.ini.temp config.ini
    (.env)$ cp config.py.temp config.py

Fill out config files.

Run
===
To run the bot do as following:

    $ ./run.sh
