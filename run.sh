#!/bin/bash
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2018-2021

export SHELL=/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
source .env/bin/activate
python3 tgchannel2toot.py
